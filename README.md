# Description
This project was provided as part of the course [Deploy and Run Apps with Docker, Kubernetes, Helm and Rancher](https://www.udemy.com/course/deploy-and-run-apps-with-docker-kubernetes-helm-rancher)
and was not written by me. I've uploaded it to gitlab so that I can reference the example kubernetes configurations. I've also included some useful rough notes from the course in this readme.

# Notes from the course

## Motivation for kubernetes
- How do we handle recovery of containers?
- How do we handle service discovery?
- How do we scale up or down specific containers and load balance them?
- What if we want to have containers on multiple hosts (distributed)?
- How do we manage configuration of the containers?
- Scheduling, how do we know when and where to add new containers?
- How do we monitor all our nodes?

## What is kubernetes?
- Container Orchestrator
- Invented by google
- Manages containerized applications and aims to:
    - Automate deployments
    - Automate scaling
    - Work across a cluster of hosts
- Framework for building distributed applications

## Useful kubectl commands:
- **kubectl get all -a-all-namespaces**
- **kubectl run [deployment-name] --image=[image-name] --replicas=5** : Runs a kubernetes deployment from the command line
- **kubectl expost deployment [deployment-name] --type=LoadBalancer --port [port-to-be-exposed] --target-port:[port-container-is-listening-on]** : Sets up a service that exposes a deployment and load balances between them
- **kubectl scale deployment nginx --replicas=3** : Updates the number of replicas for an existing deployment
- **kubectl get deployments [deployment-name] -o yaml** : Prints out the deployment configuration
- **kubectl describe service nginx** : Describes the service
- **kubectl edit deployment nginx** : Allows you to edit deployment information while deploy is running (cw to edit, escape => :wq! to quit)
- **kubectl delete deployment [deployment-name]**: Deletes the deployment
- **kubectl delete serivce [service-name]** : Deletes the service
- **kubectl get pods --output=wide** : More info about pods, IP and Node info

### Namespaces
- **kubectl create namespace webservers**
- **kubectl get namespaces** 
- **kubectl run my-nginx --image=nginx:1.15.9 --replicas=3  --namespace=webservers** : Create a deploy in a specific namespace
- **kubectl get pods --namespace=webservers** 
- **kubectl get pods --all-namespaces** 
- **kubectl get namespace webservers -o yaml** 
- **kubectl delete deployment my-nginx --namespace=webservers** 
- **kubectl delete namespace webservers** 

### Pods
- **kubectl describe pod [pod-name]**
- **kubectl get pod [pod-name] -o yaml**
- **kubectl run [pod-name] --image=[image-name] --restart=Never**
- **kubectl delete pods -all**

### Deployments
- **kubectl scale deployment [deployment-name] --replicas=15**
- **kubectl rollout undo deployment/web** : Rollbak a deployment
- **kubectl edit deployment web** : Edit deployment on the fly
- **kubectl delete deployment web** 

### Services
Each service/pod in the cluster have unique ips
Service Types:
- ClusterIP: internally accessible, communication between pods in the cluster
- NodePort: sits above ClusterIP, externally accessible, direct access to the pod
- LoadBalancer: it assumes you will integrate this with a cloud based load balancer

Commands:
- **kubectl expose deployment [deployment-name] --type=LoadBalancer --port 8080 --target-port=80**

### Ingress
- **kubectl get ingress**
- **kubectl describe ingress [ingress-name]**

### ConfigMap
ConfigMap is a place for environment variables to be defined then can be loaded into pods. 
- **kubectl describe [config-map-name]
- **kubectl logs [pod-name]
- **kubectl exec -it [pod-name] /bin/sh

#### Secrets

### Liveness and Readiness Probes

### Trouble Shooting
- **kubectl describe pod [pod-name]**

## Other useful commands
- **netstat -an | grep LISTEN** : Get list of used ports 
- **while true; do curl -s localhost:8080/version | grep nginx; sleep .5; done;** : Command to loop forever and print out the nginx version
- **while true: do kubectl get deployment [deployment-name]: sleep .5; done;**

# Could use more focus:
- StatefulSet
- Volumes
